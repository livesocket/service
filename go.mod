module gitlab.com/livesocket/service/v2

go 1.12

require (
	github.com/gammazero/nexus/v3 v3.0.0
	github.com/gempir/go-twitch-irc/v2 v2.2.1
	github.com/go-sql-driver/mysql v1.4.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.2.0 // indirect
	github.com/mattn/go-sqlite3 v1.11.0 // indirect
	gitlab.com/livesocket/conv v0.0.0-20191012101209-727e1c03a95c
	golang.org/x/crypto v0.0.0-20191108234033-bd318be0434a // indirect
	google.golang.org/appengine v1.6.5 // indirect
)
